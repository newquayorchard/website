﻿using Orchard.UI.Resources;

namespace Buzz.JPlayer
{
    public class ResourceManifest : IResourceManifestProvider
    {
        public void BuildManifests(ResourceManifestBuilder builder)
        {
            var manifest = builder.Add();
            manifest.DefineScript("jPlayer").SetUrl("jquery.jplayer.min.js").SetDependencies("jQuery");
            manifest.DefineStyle("jPlayer").SetUrl("jplayer.css");
            manifest.DefineScript("buzz.jPlayer").SetUrl("buzz.jplayer.js").SetDependencies("jPlayer");
        }
    }
}
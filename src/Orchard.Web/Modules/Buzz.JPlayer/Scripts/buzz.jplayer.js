﻿$(document).ready(function(){

    $('.jp-jplayer[data-media-file-type=mp3]').each(function () {
    
        var id = $(this).attr('id');
        var controlsId = $(this).next().attr('id');
        var mediaSrc = $(this).attr('data-media-file');
        var mediaType = $(this).attr('data-media-file-type');

        $(this).jPlayer({
            jPlayer: '#' + id ,
            cssSelectorAncestor: '#' + controlsId ,
            ready: function () {
                $(this).jPlayer("setMedia", {
                    mp3 : mediaSrc
                });

                var duration = $(this).data("jPlayer").status.duration;

                if (duration)
                {
                    $("jp-duration").html(duration);
                }
		   
            },
            swfPath: "/Modules/Buzz.Interactive/Script/Content/Swf",
        supplied: "mp3",
        wmode: "window"
    });
});

  
});
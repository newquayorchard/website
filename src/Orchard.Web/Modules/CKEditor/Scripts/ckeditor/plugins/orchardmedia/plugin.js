﻿CKEDITOR.plugins.add('orchardmedia', {
    icons: 'image',
    init: function (editor) {


        editor.addCommand('orchardmedialibrary',
            {
                exec: function (editor) {

                    sel = editor.getSelection(),
                    element = sel && sel.getSelectedElement(),
                    link = element && editor.elementPath(element).contains('a', 1);

                    var adminIndex = location.href.toLowerCase().indexOf("/admin/");
                    if (adminIndex === -1) return;
                    var url = location.href.substr(0, adminIndex) + "/Admin/Orchard.MediaLibrary?dialog=true";
                    $.colorbox({
                        href: url,
                        iframe: true,
                        reposition: true,
                        width: "90%",
                        height: "90%",
                        onLoad: function () {
                            // hide the scrollbars from the main window
                            $('html, body').css('overflow', 'hidden');
                            //$('#cboxClose').remove();
                        },
                        onClosed: function () {
                            $('html, body').css('overflow', '');

                            var selectedData = $.colorbox.selectedData;

                            if (selectedData == null) // Dialog cancelled, do nothing
                                return;

                            var newContent = '';
                            for (var i = 0; i < selectedData.length; i++) {
                                var renderMedia = location.href.substr(0, adminIndex) + "/Admin/Orchard.MediaLibrary/MediaItem/" + selectedData[i].id + "?displayType=Raw";
                                $.ajax({
                                    async: false,
                                    type: 'GET',
                                    url: renderMedia,
                                    success: function (data) {
                                        newContent += data;
                                    }
                                });
                            }

                            if (element) {
                                element.Remove();
                            }

                            //var newElement = CKEDITOR.dom.element.createFromHtml(newContent);
                            editor.insertHtml(newContent);

                        }
                    });
                },
                canUndo: false
        });

        editor.ui.addButton('OrchardMediaLibrary', {
            label: 'Insert Orchard Image',
            command: 'orchardmedialibrary',
            toolbar: 'insert',
            icon: this.path + 'icons/image.png'
        });

    }
});
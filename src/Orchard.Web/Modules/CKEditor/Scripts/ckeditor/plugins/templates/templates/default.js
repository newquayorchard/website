﻿/*
 Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.html or http://ckeditor.com/license
*/
CKEDITOR.addTemplates("default", {
    imagesPath: CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates") + "templates/images/"), templates: [
        {
    title: 'Quote',
    image: 'template3.gif',
    description: 'Blockquote formating',
    html:
    '<blockquote>' +
        '<p>Quote text here</p>' +
        '<footer>Mr Smith</footer>' +
   '</blockquote>'
        }]
});
﻿CKEDITOR.plugins.add('orchardmedia', {
    icons: 'image',
    init: function (editor) {


        editor.addCommand('orchardmediapicker', 
            {
                exec: function (editor) {

                    sel = editor.getSelection(),
                    element = sel && sel.getSelectedElement(),
                    link = element && editor.elementPath(element).contains('a', 1);

                    var data = new Object();
                    

                    if (element) {
                        data.img = new Object();
                        data.img.src = element.getAttribute('src');
                        data.img.alt = element.getAttribute('alt');
                        data.img.class = element.getAttribute('class');
                        data.img.width = element.getStyle('width').replace('px', '');
                        data.img.height = element.getStyle('height').replace('px', '');
                    }

                    var callbackName = "_pickimage_" + new Date().getTime();
                    data.callbackName = callbackName;
                    $[callbackName] = function (returnData) {

                        delete $[callbackName];

                        if (returnData.img)
                        {
                            if (!element)
                            {
                                var element = new CKEDITOR.dom.element('img');
                                editor.insertElement(element);
                            }

                            element.setAttribute('src', returnData.img.src);
                            element.setStyle('width', returnData.img.width + 'px');
                            element.setStyle('height', returnData.img.height + 'px');
                            element.setAttribute('alt', returnData.img.alt);
                            element.setAttribute('class', returnData.img.class);
                        }

                    };
                    $[callbackName].data = data;

                    var adminIndex = location.href.toLowerCase().indexOf("/admin/");
                    if (adminIndex === -1) return;
                    var url = location.href.substr(0, adminIndex)
                        + "/Admin/MediaPicker?uploadpath=" + ($(editor.element).attr("data-mediapicker-uploadpath"))
                        + "&callback=" + callbackName
                        + "&editmode=" + (!!(data.img && data.img.src))
                        + "&" + (new Date() - 0);
                    var w = window.open(url, "_blank", "width=685,height=640,status=no,toolbar=no,location=no,menubar=no,resizable=no");
                },
                canUndo: false
        });

        editor.ui.addButton('OrchardMediaPicker', {
            label: 'Insert Orchard Image',
            command: 'orchardmediapicker',
            toolbar: 'insert',
            icon: this.path + 'icons/image.png'
        });

    }
});
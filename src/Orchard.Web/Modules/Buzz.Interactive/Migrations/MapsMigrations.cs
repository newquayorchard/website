﻿using System;
using System.Collections.Generic;
using System.Data;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.MetaData;
using Orchard.ContentManagement.MetaData.Builders;
using Orchard.Core.Contents.Extensions;
using Orchard.Data.Migration;
using Orchard.Environment.Extensions;
using Buzz.Interactive.Models;

namespace Buzz.Interactive.Migrations
{
    [OrchardFeature("Buzz.Maps")]
    public class MapsMigrations : DataMigrationImpl
    {
        public int Create()
        {
            SchemaBuilder.CreateTable("MapItemPartRecord", table => table
               .ContentPartRecord()
               .Column("Latitude", DbType.Double)
               .Column("Longitude", DbType.Double)
               .Column("PolygonPoints", DbType.String)

           );

            ContentDefinitionManager.AlterPartDefinition(typeof(MapItemPart).Name, builder => builder
              .Attachable()
              .WithDescription("Provides a geo point or polygon for your item"));


            SchemaBuilder.CreateTable("MapPartRecord", table => table
                  .ContentPartRecord()
                  .Column<int>("ZoomLevel")
                  .Column<MapType>("MapType")
              );

            ContentDefinitionManager.AlterPartDefinition(typeof(MapPart).Name, builder => builder
              .Attachable()
              .WithDescription("Provides a Google maps map"));
            
            ContentDefinitionManager.AlterTypeDefinition("MapWidget", type => type
                .WithPart("CommonPart")
                .WithPart("WidgetPart")
                .WithPart("ContentPickerPart")
                .WithPart("MapPart")
                .WithSetting("Stereotype", "Widget")
                .Creatable(false)
                .Draftable(false));
            
            return 1;
        }

    }

}
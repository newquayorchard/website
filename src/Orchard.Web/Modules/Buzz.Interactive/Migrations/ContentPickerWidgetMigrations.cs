﻿using System;
using System.Collections.Generic;
using System.Data;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.MetaData;
using Orchard.ContentManagement.MetaData.Builders;
using Orchard.Core.Contents.Extensions;
using Orchard.Data.Migration;
using Orchard.Environment.Extensions;

namespace Buzz.Interactive.Migrations
{
    [OrchardFeature("Buzz.ContentPickerWidgets")]
    public class ContentPickerWidgetMigrations : DataMigrationImpl
    {
        public int Create()
        {

            SchemaBuilder.CreateTable("ContentPickerPartRecord", table => table
                .ContentPartRecord()
                .Column<string>("DisplayType", c => c.WithLength(50)));

            ContentDefinitionManager.AlterPartDefinition("ContentPickerPart", part => part
                .Attachable()
                .WithField("ContentItems", field => field
                    .OfType("ContentPickerField")
                    .WithSetting("ContentPickerFieldSettings.Hint", "Select one or more content items to render")
                    .WithSetting("ContentPickerFieldSettings.Required", "True")
                    .WithSetting("ContentPickerFieldSettings.Multiple", "True")));

            ContentDefinitionManager.AlterTypeDefinition("ContentPickerWidget", type => type
                .WithPart("CommonPart")
                .WithPart("WidgetPart")
                .WithPart("ContentPickerPart")
                .WithSetting("Stereotype", "Widget")
                .Creatable(false)
                .Draftable(false));
            
            ContentDefinitionManager.AlterPartDefinition("MediaLibraryPickerPart", part => part
               .Attachable()
               .WithField("MediaItems", field => field
                   .OfType("MediaLibraryPickerField")
                   .WithSetting("MediaLibraryPickerField.Hint", "Select one or more mdeia items to render")
                   .WithSetting("MediaLibraryPickerField.Required", "False")
                   .WithSetting("MediaLibraryPickerField.Multiple", "True")));

            ContentDefinitionManager.AlterTypeDefinition("MediaLibraryPickerWidget", type => type
                .WithPart("CommonPart")
                .WithPart("WidgetPart")
                .WithPart("MediaLibraryPickerPart")
                .WithSetting("Stereotype", "Widget")
                .Creatable(false)
                .Draftable(false));


            return 1;
        }

    }

}
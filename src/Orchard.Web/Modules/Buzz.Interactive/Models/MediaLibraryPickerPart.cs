﻿using System.Collections.ObjectModel;
using System.Linq;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Records;
using Orchard.MediaLibrary.Models;
using Orchard.MediaLibrary.Fields;
using Orchard.Environment.Extensions;
using System.ComponentModel.DataAnnotations;

namespace Buzz.Interactive.Models
{
    [OrchardFeature("Buzz.ContentPickerWidgets")]
    public class MediaLibraryPickerPart : ContentPart
    {
       
        public ReadOnlyCollection<MediaPart>  ContentItems {
            get { return ((MediaLibraryPickerField)Fields.First(x => x.Name == "MediaItems")).MediaParts.ToList().AsReadOnly(); }
        }
    }

 
}
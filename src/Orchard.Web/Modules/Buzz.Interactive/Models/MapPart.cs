﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.Records;
using Orchard.Environment.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Buzz.Interactive.Models
{

    public enum MapType {
        ROADMAP,
        SATELLITE,
        HYBRID,
        TERRAIN
    }
    [OrchardFeature("Buzz.Maps")]
    public class MapPart : ContentPart<MapPartRecord>
    {
        public int ZoomLevel {
            get { return Retrieve(r => r.ZoomLevel); }
            set { Store(r => r.ZoomLevel, value); }
        }

        public MapType MapType
        {
            get { return (MapType)Retrieve(r => r.MapType); }
            set { Store(r => r.MapType, (int)value); }
        }

    }
    [OrchardFeature("Buzz.Maps")]
    public class MapPartRecord : ContentPartRecord
    {
        public virtual int ZoomLevel { get; set; }
        public virtual int MapType { get; set; }
    }
}
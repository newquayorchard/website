using Orchard;
using Orchard.ContentManagement;

using System;
using System.Collections.Generic;
using System.Linq;

using Buzz.Maps.ViewModels;
using Orchard.Environment.Extensions;

namespace Buzz.Interactive.Models
{

    public enum MapItemType { 
        Point,
        Polygon
    }
    [OrchardFeature("Buzz.Maps")]
    public class MapItemPart : ContentPart<MapItemPartRecord> {

        public double? Latitude {
            get { return Retrieve(r => r.Latitude); }
            set { Store(r => r.Latitude,value); }
        }
        public double? Longitude {
            get { return Retrieve(r => r.Longitude); }
            set { Store(r => r.Longitude,value); }
        }

        public string PolygonPoints {
            get { return Retrieve(r => r.PolygonPoints); }
            set { Store(r => r.PolygonPoints,value); }
        }

        public MapItemType MapItemType
        {
            get {
                if (!string.IsNullOrEmpty(PolygonPoints) && !string.IsNullOrWhiteSpace(PolygonPoints))
                {
                    return MapItemType.Polygon;
                }
                else
                {
                    return MapItemType.Point;
                }
            }
        }

       
        public GeoPointViewModel GetPoint() {
            if (Latitude.HasValue && Longitude.HasValue)
            {
                return new GeoPointViewModel
                {
                    Longitude = Longitude,
                    Latitude = Latitude
                };
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<GeoPointViewModel> GetPolygon()
        {
            if (!string.IsNullOrEmpty(PolygonPoints))
            {
                return PolygonPoints.Split(new string[] { System.Environment.NewLine.ToString() }, StringSplitOptions.RemoveEmptyEntries).Select(i =>
                {
                    string[] pointParts = i.Split(';');
                    double lon;
                    double lat;

                    if (double.TryParse(pointParts[0], out lon) && double.TryParse(pointParts[1], out lat))
                    {
                        return new GeoPointViewModel
                        {
                            Longitude = lon,
                            Latitude = lat
                        };
                    }
                    else
                    {
                        return null;
                    }

                }).ToList();
            }
            else
            {
                return null;
            }
        }
        

    }
}

using Orchard;
using Orchard.ContentManagement.Records;
using Orchard.Environment.Extensions;

namespace Buzz.Interactive.Models
{
    [OrchardFeature("Buzz.Maps")]
    public class MapItemPartRecord : ContentPartRecord {
        public virtual double? Latitude{ get; set; }
        public virtual double? Longitude{ get; set; }

        public virtual string PolygonPoints { get; set; }

    }
}
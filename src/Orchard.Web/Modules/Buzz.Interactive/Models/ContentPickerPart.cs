﻿using System.Collections.ObjectModel;
using System.Linq;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Records;
using Orchard.ContentPicker.Fields;
using Orchard.Environment.Extensions;

namespace Buzz.Interactive.Models {
    [OrchardFeature("Buzz.ContentPickerWidgets")]
    public class ContentPickerPart : ContentPart<ContentPickerPartRecord> {
        public string DisplayType {
            get { return Retrieve(r => r.DisplayType); }
            set { Store(r => r.DisplayType,value); }
        }

        public ReadOnlyCollection<ContentItem>  ContentItems {
            get { return ((ContentPickerField) Fields.First(x => x.Name == "ContentItems")).ContentItems.ToList().AsReadOnly(); }
        }
    }
    [OrchardFeature("Buzz.ContentPickerWidgets")]
    public class ContentPickerPartRecord : ContentPartRecord {
        public virtual string DisplayType { get; set; }
    }
}
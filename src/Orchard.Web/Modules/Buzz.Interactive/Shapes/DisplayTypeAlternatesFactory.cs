﻿using Orchard.DisplayManagement.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Buzz.Interactive.Shapes
{
    public class DisplayTypeAlternatesFactory : ShapeDisplayEvents 
    {
        public override void Displaying(ShapeDisplayingContext context)
        {
            context.ShapeMetadata.OnDisplaying(displayedContext =>
            {
                var shapeType = displayedContext.ShapeMetadata.Type;
                var contentItem = displayedContext.Shape.ContentItem;
                var displayType = displayedContext.ShapeMetadata.DisplayType;
                var contentType = contentItem.ContentType;
                displayedContext.ShapeMetadata.Alternates.Add(
                    String.Format("{0}__{1}", shapeType, displayType));
                displayedContext.ShapeMetadata.Alternates.Add(
                    String.Format("{0}__{1}__{2}", shapeType, (string)contentType, displayType));
            });
        }
    }
}
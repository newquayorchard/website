﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Buzz.Maps.ViewModels
{
    [DataContract(Name = "gp")]
    public class GeoPointViewModel
    {
        [DataMember(Name = "latitude")]
        public double? Latitude { get; set; }
        [DataMember(Name = "longitude")]
        public double? Longitude { get; set; }
    }
}
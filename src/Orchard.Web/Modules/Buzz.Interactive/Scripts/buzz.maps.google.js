﻿var app = angular.module('BuzzMaps', ['ngSanitize']);

function httpError(status) {

}

var BuzzMapsCtrl = function ($scope, $http, $timeout) {

    var map;
    var mapItems = new Array();
    var currentInfoWindow;
    var mapDiv = document.getElementById('map-canvas-display');
    var mapType =  $(mapDiv).data('mapType');
    var initialZoom = $(mapDiv).data('zoom');

    function initialize() {
      
        var mapOptions = {
            zoom: initialZoom,
            center: new google.maps.LatLng(50.4403918461249, -4.992856979370117),
            mapTypeId: google.maps.MapTypeId[mapType],
            panControl: true,
            panControlOptions: {
                position: google.maps.ControlPosition.TOP_LEFT
            },
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.TOP_RIGHT
            },
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL,
                position: google.maps.ControlPosition.TOP_LEFT
            },
            draggable : false,
            streetViewControl: false
        }
        map = new google.maps.Map(mapDiv, mapOptions);
    }

    initialize();


    $scope.results = new Array();

    if (window.mapItemsArray)
    {
        $scope.results = window.mapItemsArray;
        updateMarkers($scope.results.slice());
    }

function selectItem(index) {
        var mapItem = mapItems[index];
        $scope.selectedItem = mapItem;
    }

function mapMarkerClick(marker, index) {
        google.maps.event.addListener(marker, 'click', function () {
            $scope.$apply(function () {
                selectItem(index);
            });
        });
    }


$scope.clearResults = function () {
    hideMarkers();
    $scope.results = new Array();
    $scope.showResultsArea = false;


};

function hideMarkers() {
  
    while (mapItems.length) {
        var item = mapItems.pop();
        if (item.marker)
        {
            item.marker.setMap(null);
        }

        if (item.polygon)
        {
            item.polygon.setMap(null);
        }
        
    }

    if (currentInfoWindow)
    {
        currentInfoWindow.close();
    }
}

function updateMarkers(items)
{

    hideMarkers();

    if (items.length > 0) {
  
        for (var i = 0; i < items.length; i++) {
            var item = items[i];

            if (item.point) {
                var latLng = new google.maps.LatLng(item.point.latitude, item.point.longitude);
                var marker = new google.maps.Marker({
                    position: latLng,
                    map: map
                });

                var mapItem = new Object();
                mapItem.marker = marker;
                mapItem.item = items[i];
                mapItems.push(mapItem);

                mapMarkerClick(marker, i);
            }
            else if (item.polygon) {
                var polyPoints = new Array();

                for (var c = 0; c < item.polygon.length; c++) {
                    var latLng = new google.maps.LatLng(item.polygon[c].latitude, item.polygon[c].longitude);
                    polyPoints.push(latLng);
                }

                var custShape = new google.maps.Polygon({
                    paths: polyPoints,
                    strokeColor: '#ff0000',
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: '#ff0000',
                    fillOpacity: 0.35
                });

                custShape.setMap(map);

                var mapItem = new Object();
                mapItem.polygon = custShape;
                mapItem.item = items[i];
                mapItems.push(mapItem);

                mapMarkerClick(custShape, i);
            }
        }
        updateBounds();

        if (mapItems.length == 1)
        {
            selectItem(0);
        }
    }
}

function updateBounds() {
    var markerBounds = new google.maps.LatLngBounds();
    var itemsDisplayed = 0;

    _.each(mapItems, function (mapItem) {

        if (mapItem.marker) {
            markerBounds.extend(mapItem.marker.position);
        }
        else if (mapItem.polygon)
        {
            for (var c = 0; c < item.polygon.length; c++) {
                var latLng = new google.maps.LatLng(item.polygon[c].latitude, item.polygon[c].longitude);
                markerBounds.extend(latLng);
            }
        }
            itemsDisplayed++;
    });

    if (itemsDisplayed == 0) {
        markerBounds.extend(new google.maps.LatLng(50.4403918461249, -4.992856979370117));
    }


    google.maps.event.addListener(map, 'zoom_changed', function () {

        google.maps.event.addListenerOnce(map, 'bounds_changed', function (event) {
            if (this.getZoom() > initialZoom && this.initialZoom == true) {
                // Change max/min zoom here
                this.setZoom(initialZoom);
                this.initialZoom = false;
            }
        });

    });
    map.initialZoom = true;
    map.fitBounds(markerBounds);
}
 
$scope.$watch('selectedItem', function (oldValue, newValue) {


    if ($scope.selectedItem && ($scope.selectedItem.marker || $scope.selectedItem.polygon)) {

        if (currentInfoWindow) {
            currentInfoWindow.close();
        }


        $timeout(function () {

            $('#map-item-template-wrapper').show();

            var sampleheight = $('#map-item-template').height();
            $('#map-item-template-wrapper').hide();

            var sampleoffset = -sampleheight - 108;

            var contentString = $('#map-item-template').html();
            var infowindow = new InfoBox({
                content: contentString,
                boxStyle: {
                    height: sampleheight
                },
                closeBoxURL: "/modules/buzz.interactive/content/images/icon-map-close.png",
                pixelOffset: new google.maps.Size(0, sampleoffset)
            });



            if ($scope.selectedItem.marker) {
                //infowindow.open(map, $scope.selectedItem.marker);
            }
            else if ($scope.selectedItem.polygon) {
                var point = GetPolygonCenter($scope.selectedItem.item.polygon);

                var marker = new google.maps.Marker({
                    position: point,
                    map: map,
                    icon: ''
                });

                marker.setVisible(false);

                infowindow.open(map, marker);
            }

            google.maps.event.addListener(infowindow, 'closeclick', function () {
                $scope.$apply(function () {
                    $scope.selectedItem = null;
                });
            });

            currentInfoWindow = infowindow;


        }, 10);


    }
});

function GetPolygonCenter(polygon)
{
    var minLat;
    var maxLat;
    var minLon;
    var maxLon;

    for (var i = 0; i < polygon.length ; i++)
    {
        if (!minLat) {
            minLat = polygon[i].latitude;
            maxLat = polygon[i].latitude;

            minLon = polygon[i].longitude;
            maxLon = polygon[i].longitude;
        }
        else {

            minLat = Math.min(minLat, polygon[i].latitude);
            maxLat = Math.max(maxLat, polygon[i].latitude);

            minLon = Math.min(minLon, polygon[i].longitude);
            maxLon = Math.max(maxLon, polygon[i].longitude);
        }

       
    }
 var ret = new google.maps.LatLng(minLat + ((maxLat - minLat) / 2), minLon + ((maxLon - minLon) / 2));
        //ret.latitude = minLat + ((maxLat - minLat) / 2);
        //ret.longitude = minLon + ((maxLon - minLon) / 2);
        return ret;


}


};

BuzzMapsCtrl.$inject = ['$scope', '$http', '$timeout'];
﻿
var resultTarget = '';
var resultType = '';

var polyPoints = [];

var lastPoly;
var lastPoint;

var targetFieldset;
var newline = '\n';
var map;


$('#map-data fieldset button').click(function () {
    $(this).siblings('input,textarea').val("");
    clearMap();
    polyPoints = [];
    return false;
});

$('.map-result').click(function () {
    setFieldSet($(this));
});

function setFieldSet(radio) {
    targetFieldset = radio.parent().next();
    resultType = radio.val();

    $('#map-data fieldset').attr('readonly', true);
    targetFieldset.removeAttr('readonly');
    $('#map-data fieldset[readonly] input,#map-data fieldset[readonly] textarea').val("");
    clearMap();

    switch (resultType) {
        case 'point':
            updatePoint();
            break;
        case 'polygon':
            var polystring = targetFieldset.find('textarea').first().val();
            var polyPairs = polystring.split(newline);
            polyPoints = [];
            for (var i = 0; i < polyPairs.length ; i++) {
                if (polyPairs[i] != '' && polyPairs[i].indexOf(';') > -1) {
                    pointParts = polyPairs[i].split(';');
                    polyPoints.push(new google.maps.LatLng(pointParts[1], pointParts[0]));
                }
            }
            updatePoly();
    }

}

var mapZoom;


function initialize() {
    var mapDiv = document.getElementById('map-canvas');
    map = new google.maps.Map(mapDiv, {
        center: new google.maps.LatLng(51.15798779237731, -2.08740234375),
        zoom: 7,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    google.maps.event.addListener(map, 'click', function (event) {

        mapZoom = map.getZoom();

        setTimeout(function () { updatePosition(event) }, 200);

    });

    var typeRadio = $('.map-result:checked').first();
    setFieldSet(typeRadio);

}


function updatePosition(event) {
    if (mapZoom == map.getZoom()) {
        switch (resultType) {
            case 'polygon':
                polyPoints.push(event.latLng);

                updatePoly();
                break;

            case 'point':
                targetFieldset.find('input.longitude').first().val(event.latLng.lng());
                targetFieldset.find('input.latitude').first().val(event.latLng.lat());
                updatePoint();
                break;
        }
    }
}


function clearMap() {
    if (lastPoly) {
        lastPoly.setMap(null);
    }

    if (lastPoint) {
        lastPoint.setMap(null);
    }
}


function updatePoint() {

    var longitude = targetFieldset.find('input.longitude').first().val();
    var latitude = targetFieldset.find('input.latitude').first().val();

    var latLng = new google.maps.LatLng(latitude,
                                        longitude);
    var marker = new google.maps.Marker({
        position: latLng,
        map: map
    });

    if (lastPoint) {
        lastPoint.setMap(null);
    }

    lastPoint = marker;
}

function updatePoly() {

    var polyString = '';
    for (var i = 0; i < polyPoints.length; i++) {
        polyString += polyPoints[i].lng() + ';' + polyPoints[i].lat() + newline;
    }
    targetFieldset.find('textarea').val(polyString);

    if (polyPoints.length > 1) {

        var tempPolyPoints = polyPoints.slice();
        var firstPoint = tempPolyPoints[0];
        tempPolyPoints.push(firstPoint);

        var custShape = new google.maps.Polygon({
            paths: tempPolyPoints,
            strokeColor: '#ff0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#ff0000',
            fillOpacity: 0.35
        });

        custShape.setMap(map);

        if (lastPoly) {
            lastPoly.setMap(null);
        }
        lastPoly = custShape;
    }
}

initialize();
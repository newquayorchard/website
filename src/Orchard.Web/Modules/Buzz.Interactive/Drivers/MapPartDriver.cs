using JetBrains.Annotations;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.Localization;
using Orchard.UI.Notify;
using Buzz.Interactive.Models;
using Orchard.Environment.Extensions;

namespace Buzz.Interactive.Drivers
{
    [OrchardFeature("Buzz.Maps")]
    public class MapPartDriver : ContentPartDriver<MapPart>
    {

        private const string TemplateName = "Parts/Map";

        public Localizer T { get; set; }

        public MapPartDriver()
        {
            T = NullLocalizer.Instance;
        }

        protected override DriverResult Display(MapPart part, string displayType, dynamic shapeHelper)
        {
            return ContentShape("Parts_Map",
                () => shapeHelper.Parts_Map(ZoomLevel: part.ZoomLevel, MapType: part.MapType));
        }

        protected override DriverResult Editor(MapPart part, dynamic shapeHelper)
        {
            return ContentShape("Parts_Map_Edit",
                    () => shapeHelper.EditorTemplate(TemplateName: TemplateName, Model: part, Prefix: Prefix));
        }

        protected override DriverResult Editor(MapPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            updater.TryUpdateModel(part, Prefix, null, null);
            return Editor(part, shapeHelper);
        }

    }
}
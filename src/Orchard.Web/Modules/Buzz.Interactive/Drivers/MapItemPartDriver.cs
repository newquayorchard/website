using JetBrains.Annotations;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.Localization;
using Orchard.UI.Notify;
using Buzz.Interactive.Models;
using Orchard.Environment.Extensions;

namespace Buzz.Interactive.Drivers
{
    [OrchardFeature("Buzz.Maps")]
    public class MapItemPartDriver : ContentPartDriver<MapItemPart>
    {

        private const string TemplateName = "Parts/MapItem";

        public Localizer T { get; set; }

        public MapItemPartDriver()
        {
            T = NullLocalizer.Instance;
        }

        protected override DriverResult Display(MapItemPart part, string displayType, dynamic shapeHelper)
        {
            return ContentShape("Parts_MapItem",
                () => shapeHelper.Parts_MapItem(Longitude: part.Longitude, Latitude: part.Latitude, Polygonpoints : part.PolygonPoints, MapItemType : part.MapItemType, Part: part));
        }

        protected override DriverResult Editor(MapItemPart part, dynamic shapeHelper)
        {
            return ContentShape("Parts_MapItem_Edit",
                    () => shapeHelper.EditorTemplate(TemplateName: TemplateName, Model: part, Prefix: Prefix));
        }

        protected override DriverResult Editor(MapItemPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            updater.TryUpdateModel(part, Prefix, null, null);
            return Editor(part, shapeHelper);
        }

    }
}
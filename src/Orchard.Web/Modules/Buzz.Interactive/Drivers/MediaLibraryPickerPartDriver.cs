﻿
using Buzz.Interactive.Models;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;
using Orchard.Environment.Extensions;
using Orchard.Localization;
using Orchard.MediaLibrary.Fields;
using System.Linq;

namespace Buzz.Interactive.Drivers
{
    [OrchardFeature("Buzz.ContentPickerWidgets")]
    public class MediaLibraryPickerPartDriver : ContentPartDriver<MediaLibraryPickerPart>
    {
        private readonly IContentManager _contentManager;

        public MediaLibraryPickerPartDriver(IContentManager contentManager)
        {
            _contentManager = contentManager;
            T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        protected override string Prefix {
            get { return "MediaLibraryPicker"; }
        }

        protected override DriverResult Display(MediaLibraryPickerPart part, string displayType, dynamic shapeHelper)
        {

            var list = shapeHelper.List();
            var itemDisplayType = "Summary";

            MediaLibraryPickerField field = (MediaLibraryPickerField)part.Fields.FirstOrDefault(f => f.Name == "MediaItems");

            if (field != null)
            {
                list = field.MediaParts.Select(mp => _contentManager.BuildDisplay(mp, itemDisplayType));
            }
            
            return ContentShape("Parts_MediaLibraryPicker", () =>
            shapeHelper.Parts_MediaLibraryPicker(MediaItems: list)
           );

            /*
            foreach (var contentItem in part.ContentItems)
            {
                var contentShape = _contentManager.BuildDisplay(contentItem, itemDisplayType);
                list.Add(contentShape);
            }

            return ContentShape("Parts_MediaLibraryPicker", () =>
             shapeHelper.Parts_MediaLibraryPicker(ContentItems: list)
            );*/
        }
    }
}
﻿using Buzz.Interactive.Models;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;
using Orchard.Environment.Extensions;
using Orchard.Localization;

namespace Buzz.Interactive.Drivers {
    [OrchardFeature("Buzz.ContentPickerWidgets")]
    public class ContentPickerPartDriver : ContentPartDriver<ContentPickerPart> {
        private readonly IContentManager _contentManager;

        public ContentPickerPartDriver(IContentManager contentManager) {
            _contentManager = contentManager;
            T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        protected override string Prefix {
            get { return "ContentPicker"; }
        }
        
        protected override DriverResult Display(ContentPickerPart part, string displayType, dynamic shapeHelper) {
            return ContentShape("Parts_ContentPicker", () => {
                var list = shapeHelper.List();
                var itemDisplayType = string.IsNullOrWhiteSpace(part.DisplayType) ? "Summary" : part.DisplayType.Trim();
                foreach (var contentItem in part.ContentItems) {
                    var contentShape = _contentManager.BuildDisplay(contentItem, itemDisplayType);
                    list.Add(contentShape);
                }
                return shapeHelper.Parts_ContentPicker(ContentItems: list);
            });
        }

        protected override DriverResult Editor(ContentPickerPart part, dynamic shapeHelper) {
            return ContentShape("Parts_ContentPicker_Edit", () => shapeHelper.EditorTemplate(TemplateName: "Parts/ContentPicker", Model: part, Prefix: Prefix));
        }

        protected override DriverResult Editor(ContentPickerPart part, IUpdateModel updater, dynamic shapeHelper) {
            updater.TryUpdateModel(part, Prefix, null, null);
            return Editor(part, shapeHelper);
        }

        protected override void Importing(ContentPickerPart part, ImportContentContext context) {
            context.ImportAttribute(part.PartDefinition.Name, "DisplayType", s => part.DisplayType = s);
        }

        protected override void Exporting(ContentPickerPart part, ExportContentContext context) {
            context.Element(part.PartDefinition.Name).SetAttributeValue("DisplayType", part.DisplayType);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Buzz.Interactive.Utils
{
    public static class EnumHelpers
    {
        public static SelectList ToSelectList(this Enum enumeration)
        {
            var list = (from Enum d in Enum.GetValues(enumeration.GetType())
                        select new { ID = (int)Enum.Parse(enumeration.GetType(), Enum.GetName(enumeration.GetType(), d)), Name = DeCamelise(d.ToString().Replace("_", " ").Replace("__", "-")) }).ToList();
            SelectList selectList = new SelectList(list, "ID", "Name", (int)Enum.Parse(enumeration.GetType(), Enum.GetName(enumeration.GetType(), enumeration)));
            return selectList;
        }

        private static string DeCamelise (string inputString)
        {
            return Regex.Replace(inputString, "(\\B[A-Z])", " $1");
        }
    }
}
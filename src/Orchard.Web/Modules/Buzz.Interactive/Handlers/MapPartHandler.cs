﻿using Orchard.ContentManagement.Handlers;
using Buzz.Interactive.Models;
using Orchard.Data;
using Orchard.Environment.Extensions;

namespace Buzz.Interactive.Handlers {
    [OrchardFeature("Buzz.Maps")]
    public class MapPartHandler : ContentHandler {
        public MapPartHandler(IRepository<MapPartRecord> repository)
        {
            Filters.Add(StorageFilter.For(repository));
        }
    }
}
﻿using Orchard.ContentManagement.Handlers;
using Buzz.Interactive.Models;
using Orchard.Data;
using Orchard.Environment.Extensions;

namespace Buzz.Interactive.Handlers {
    [OrchardFeature("Buzz.ContentPickerWidgets")]
    public class ContentPickerPartHandler : ContentHandler {
        public ContentPickerPartHandler(IRepository<ContentPickerPartRecord> repository) {
            Filters.Add(StorageFilter.For(repository));
        }
    }
}
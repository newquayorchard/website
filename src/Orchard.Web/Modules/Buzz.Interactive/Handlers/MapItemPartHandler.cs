﻿using Orchard.ContentManagement.Handlers;
using Buzz.Interactive.Models;
using Orchard.Data;
using Orchard.Environment.Extensions;

namespace Buzz.Interactive.Handlers {
    [OrchardFeature("Buzz.Maps")]
    public class MapItemPartHandler : ContentHandler {
        public MapItemPartHandler(IRepository<MapItemPartRecord> repository)
        {
            Filters.Add(StorageFilter.For(repository));
        }
    }
}
﻿using Orchard.UI.Resources;

namespace Buzz.Interactive
{
    public class ResourceManifest : IResourceManifestProvider
    {
        public void BuildManifests(ResourceManifestBuilder builder)
        {
            var manifest = builder.Add();
            manifest.DefineScript("GoogleMapsV3").SetUrl("http://maps.googleapis.com/maps/api/js?key=AIzaSyDMovP1mBDBmkl5ySfjNS3O17DxjQuBhFc&sensor=false").SetDependencies("jQuery");
            manifest.DefineScript("AngularJS").SetUrl("//ajax.googleapis.com/ajax/libs/angularjs/1.1.5/angular.min.js");
            manifest.DefineScript("AngularSanitize").SetUrl("http://code.angularjs.org/1.1.2/angular-sanitize.js").SetDependencies("AngularJS");
            manifest.DefineScript("UnderscoreJS").SetUrl("https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.4.4/underscore-min.js").SetDependencies("jQuery");
            manifest.DefineStyle("fontAwsome").SetUrl("font-awesome.css");
 
        }
    }
}
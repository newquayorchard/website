﻿using System;
using System.Linq;
using System.Collections.Generic;

using Orchard.Widgets.Handlers;
using Orchard.Widgets.Services;
using Orchard;
using Orchard.Environment.Extensions;


namespace Buzz.Interactive.RuleEngine
{
    [OrchardFeature("Buzz.LayerRules")]
    public class QuerystringRuleProvider : IRuleProvider {

        private readonly IWorkContextAccessor _workContextAccessor;
        public QuerystringRuleProvider(IWorkContextAccessor workContextAccessor)
        {
            _workContextAccessor = workContextAccessor;
        }

        public void Process(RuleContext ruleContext) { 
            if (!String.Equals(ruleContext.FunctionName, "querystring", StringComparison.OrdinalIgnoreCase)) {
                return;
            }

            string key = Convert.ToString(ruleContext.Arguments[0]);
            var querystring = _workContextAccessor.GetContext().HttpContext.Request.QueryString;

            //if only one argument provided we'll just check that the key exists
            if (ruleContext.Arguments.Length == 1)
            {
                ruleContext.Result = querystring.AllKeys.Contains(key);
                return;
            }
            
            string value = Convert.ToString(ruleContext.Arguments[1]);
            string querystringValue = querystring[key].ToString();

            ruleContext.Result = (querystring.AllKeys.Contains(key) && String.Equals(value, querystringValue, StringComparison.OrdinalIgnoreCase));
        }
    }
}
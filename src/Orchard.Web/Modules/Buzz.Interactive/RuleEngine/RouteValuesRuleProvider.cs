﻿using System;
using System.Linq;
using System.Collections.Generic;

using Orchard.Widgets.Handlers;
using Orchard.Widgets.Services;
using Orchard;
using Orchard.Environment.Extensions;


namespace Buzz.Interactive.RuleEngine
{
    [OrchardFeature("Buzz.LayerRules")]
    public class ContentDisplayedRuleProvider : IRuleProvider {

        private readonly IWorkContextAccessor _workContextAccessor;
        public ContentDisplayedRuleProvider(IWorkContextAccessor workContextAccessor)
        {
            _workContextAccessor = workContextAccessor;
        }

        public void Process(RuleContext ruleContext) { 
            if (!String.Equals(ruleContext.FunctionName, "routevalue", StringComparison.OrdinalIgnoreCase)) {
                return;
            }

            string key = Convert.ToString(ruleContext.Arguments[0]);
            var routeData = _workContextAccessor.GetContext().HttpContext.Request.RequestContext.RouteData;

            //if only one argument provided we'll just check that the key exists
            if (ruleContext.Arguments.Length == 1)
            {
                ruleContext.Result = routeData.Values.Keys.Contains(key);
                return;
            }
            
            string value = Convert.ToString(ruleContext.Arguments[1]);
            string routeValue = routeData.Values[key].ToString();

            ruleContext.Result = (routeData.Values.Keys.Contains(key) && String.Equals(value, routeValue, StringComparison.OrdinalIgnoreCase));
        }
    }
}